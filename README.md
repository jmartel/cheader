# 🧀 cheader : Never update your prototypes again !!

Here is a small python3 script to automaticly update functions prototypes in your header files.
Result is already 42 norm compliant.

It can be manually launched or integrated to your Makefile.

## How to setup
- Download header.py file
- Copy it to your project directory
- Go at the end of header.py file to configure it

You need to add a call to `automatic_header` function for each .h you want.

automatic_header take 4 arguments:
- directory path (as a string) : path to the directory containing source code
- header path (as a string) : path to the header file
- tab_offset (as an int) : change it norminette return global scope bad aligned in header
- recursive_discover (as a boolean) : does it explore subdirectories in in directory path

## How to launch manually :
`python3 header.py`

## How to integrate to your Makefile
You shall modify your all rule to:

>all:
>
>	@python3 header.py
>
>	@make $(NAME)

## How it works ?

The script will add a delimiter in headers files, and it will always write under it.
Everything below this delimiter will be preserved.

Delimiter looks like this :

>\/*
>
>\********************************************************************************
>
>*/

Your header shall looks like this :

>\#ifndef ..
>
>\# define ....
>
> Includes, Typedefs, etc
>
>\/*
>
>\********************************************************************************
>
>*/
>
>#endif

Prototypes would automaticly be added after delimiter.
For first use, you can ommit delimiter.
